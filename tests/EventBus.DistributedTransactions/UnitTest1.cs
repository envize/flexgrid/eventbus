global using Xunit;
using Envize.FlexGrid.EventBus.DistributedTransactions.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public class UnitTest1
{
    private readonly IConfiguration _configuration;

    public UnitTest1()
    {
        _configuration = new ConfigurationBuilder()
            .AddJsonFile("secrets.json", optional: true)
            .Build();
    }

    [Theory]
    [MemberData(nameof(SnapshotStores))]
    public async Task FiresAndForgets(SnapshotStore store)
    {
        var transactionRunner = GetRunner(store);

        await transactionRunner.FireAndForget(TestTransactionController.Key, false);
    }

    [Theory]
    [MemberData(nameof(SnapshotStores))]
    public async Task RunsCorrectly(SnapshotStore store)
    {
        var transactionRunner = GetRunner(store);

        var result = await transactionRunner.Run<string>(TestTransactionController.Key, false);

        Assert.NotNull(result);
        Assert.True(char.IsDigit(result.First()));
        Assert.True(char.IsLetter(result.Last()));
    }

    [Theory]
    [MemberData(nameof(SnapshotStores))]
    public async Task RollsBack(SnapshotStore store)
    {
        var transactionRunner = GetRunner(store);

        async Task Act() => await transactionRunner.Run<string>(TestTransactionController.Key, true);

        await Assert.ThrowsAsync<TransactionFailedException>(Act);
    }

    private ITransactionRunner GetRunner(SnapshotStore store = SnapshotStore.InMemory)
    {
        var provider = CreateProvider(store);
        return provider.GetRequiredService<ITransactionRunner>();
    }

    private IServiceProvider CreateProvider(SnapshotStore store)
    {
        var values = new Dictionary<string, string>()
        {
#if DEBUG
            {$"{MongoOptions.SectionName}:{nameof(MongoOptions.ConnectionString)}", _configuration["ConnectionString"]},
            {$"{MongoOptions.SectionName}:{nameof(MongoOptions.DatabaseName)}", "transactions"},
#endif
            {$"{TransactionOptions.SectionName}:{nameof(TransactionOptions.SnapshotStore)}", store.ToString()}
        };

        var configuration = new ConfigurationBuilder()
            .AddInMemoryCollection(values)
            .Build();

        return new ServiceCollection()
            .AddLogging()
            .AddDistributedTransactions<IAssemblyMarker, MockEventbus>(configuration)
            .BuildServiceProvider();
    }

    public static IEnumerable<object[]> SnapshotStores => new[]
    {
        new object[] {SnapshotStore.InMemory},
#if DEBUG
        new object[] {SnapshotStore.Mongo}
#endif
    };
}
