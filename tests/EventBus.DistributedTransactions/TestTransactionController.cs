namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class TestTransactionController : ITransactionController
{
    public const string Key = "test";

    public TransactionDetails Define()
    {
        return new(
            key: Key,
            initialState: TestState.Initial,
            endState: TestState.Completed,
            events: new TransactionDetails.Event[]
            {
                new(TestState.Initial, TestState.Intermediate),
                new(TestState.Intermediate, TestState.Completed)
            }
        );
    }
}

internal static class TestState
{
    public const string Initial = "initial";
    public const string Intermediate = "intermediate";
    // public const string CreateError = "create-error";
    public const string Completed = "completed";
}
