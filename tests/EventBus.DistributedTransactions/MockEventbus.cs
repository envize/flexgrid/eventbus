namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class MockEventbus : IEventPublisher,
    IEventCompletionPublisher,
    IEventFailurePublisher,
    IRollbackPublisher,
    IRollbackCompletionPublisher
{
    private readonly IEventReceiver _receiver;
    private readonly IEventCompletionReceiver _completionReceiver;
    private readonly IEventFailureReceiver _failureReceiver;
    private readonly IRollbackReceiver _rollbackReceiver;
    private readonly IRollbackCompletionReceiver _rollbackCompletionReceiver;

    public MockEventbus(
        IEventReceiver receiver,
        IEventCompletionReceiver completionReceiver,
        IEventFailureReceiver failureReceiver,
        IRollbackReceiver rollbackReceiver,
        IRollbackCompletionReceiver rollbackCompletionReceiver
    )
    {
        _receiver = receiver;
        _completionReceiver = completionReceiver;
        _failureReceiver = failureReceiver;
        _rollbackReceiver = rollbackReceiver;
        _rollbackCompletionReceiver = rollbackCompletionReceiver;
    }

    public Task Publish(EventMessage message)
    {
        Task.Run(async () => await _receiver.Receive(message));

        return Task.CompletedTask;
    }

    public Task Publish(EventCompletionMessage message)
    {
        Task.Run(async () => await _completionReceiver.Receive(message));

        return Task.CompletedTask;
    }

    public Task Publish(EventFailureMessage message)
    {
        Task.Run(async () => await _failureReceiver.Receive(message));

        return Task.CompletedTask;
    }

    public Task Publish(RollbackMessage message)
    {
        Task.Run(async () => await _rollbackReceiver.Receive(message));

        return Task.CompletedTask;
    }

    public Task Publish(RollbackCompletionMessage message)
    {
        Task.Run(async () => await _rollbackCompletionReceiver.Receive(message));

        return Task.CompletedTask;
    }
}
