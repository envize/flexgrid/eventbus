namespace Envize.FlexGrid.EventBus.DistributedTransactions;

[TransactionEvent(TestTransactionController.Key, TestState.Initial)]
internal class InitialTransactionEventHandler : ITransactionEventHandler<bool, int>
{
    public Task<int> Handle(bool shouldFail)
    {
        var result = shouldFail ? 99 : new Random().Next(26);

        return Task.FromResult(result);
    }

    public Task Rollback(bool shouldFail, int result)
    {
        return Task.CompletedTask;
    }
}

[TransactionEvent(TestTransactionController.Key, TestState.Intermediate)]
internal class IntermediateTransactionEventHandler : ITransactionEventHandler<int, string>
{
    private const string _alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public Task<string> Handle(int letterIndex)
    {
        var result = $"{letterIndex + 1}{_alphabet[letterIndex]}";

        return Task.FromResult(result);
    }

    public Task Rollback(int letterIndex, string result)
    {
        return Task.CompletedTask;
    }
}

// [TransactionEvent(TestTransactionController.Key, TestState.CreateError)]
// internal class CreateErrorTransactionEventHandler : ITransactionEventHandler<string, string>
// {
//     private const bool _shouldThrowError = false;
//     private const int _errorRate = 100;

//     public Task<string> Handle(string payload)
//     {
//         if (new Random().Next(_shouldThrowError ? 1 : _errorRate) == 0)
//         {
//             throw new Exception("Big oopsie!");
//         }

//         return Task.FromResult(payload);
//     }

//     public Task Rollback(string payload, string result)
//     {
//         throw new NotImplementedException();
//     }
// }
