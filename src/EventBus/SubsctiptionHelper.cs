using System;
using System.Linq;

namespace Envize.FlexGrid.EventBus
{
    internal static class SubscriptionHelper
    {
        public static Subscription CreateFromTypes(Type eventType, Type handlerType)
        {
            if (!IsEventTypeValid(eventType) ||
                !IsHandlerTypeValid(handlerType, eventType))
            {
                // todo: implement exception
                throw new NotImplementedException();
            }

            var @event = (IntegrationEvent?) Activator.CreateInstance(eventType);
            string eventname = @event!.Name;

            var subscription = new Subscription(eventname, eventType, handlerType);
            return subscription;
        }

        // reflection variant of `where TEvent : IntegrationEvent, new()`
        private static bool IsEventTypeValid(Type eventType)
        {
            return typeof(IntegrationEvent).IsAssignableFrom(eventType) &&
                eventType.GetConstructor(Type.EmptyTypes) != null;
        }

        // reflection variant of `where THandler : IIntegrationEventHandler<TEvent>`
        private static bool IsHandlerTypeValid(Type handlerType, Type eventType)
        {
            // ! not sure if this works
            // todo: test this in isolation
            return handlerType.GetInterfaces().Any(@interface => 
                @interface.IsGenericType && 
                @interface.GetGenericTypeDefinition() == typeof(IIntegrationEventHandler<>)) &&
                GetEventTypeFromHandlerType(handlerType) == eventType;
        }

        public static Type GetEventTypeFromHandlerType(Type handlerType) 
        {
            return handlerType.GetInterfaces()
                .Where(MatchesHandlerInterface)
                .Select(@interface => @interface.GetGenericArguments().First())
                .First();
        }

        public static bool MatchesHandlerInterface(Type interfaceType)
        {
            return interfaceType.IsGenericType &&
                interfaceType.GetGenericTypeDefinition() == typeof(IIntegrationEventHandler<>);
        }
    }
}