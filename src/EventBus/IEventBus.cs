namespace Envize.FlexGrid.EventBus;

public interface IEventBus : IAsyncDisposable
{
    Task Publish<TEvent>(TEvent @event)
        where TEvent : IntegrationEvent, new();

    Task Subscribe(Type eventType, Type handlerType);
}
