namespace Envize.FlexGrid.EventBus;

public record Subscription(string EventName, Type EventType, Type HandlerType)
{
}
