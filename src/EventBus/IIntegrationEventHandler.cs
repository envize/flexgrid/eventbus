using System.Threading.Tasks;

namespace Envize.FlexGrid.EventBus
{
    public interface IIntegrationEventHandler<TEvent>
        where TEvent : IntegrationEvent, new()
    {
        Task Handle(TEvent @event);
    }
}
