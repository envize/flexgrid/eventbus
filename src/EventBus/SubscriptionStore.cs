namespace Envize.FlexGrid.EventBus;

// todo: check for duplicate subscriptions
public class SubscriptionStore
{
    private List<Subscription> _subscriptions = new();

    public IEnumerable<Type> GetEventHandlers(string eventName)
    {
        return _subscriptions
            .Where(s => s.EventName == eventName)
            .Select(s => s.HandlerType);
    }

    public IDictionary<string, List<Subscription>> GetSubscriptions()
    {
        return _subscriptions
            .GroupBy(s => s.EventName)
            .ToDictionary(group => group.Key, group => group.ToList());
    }

    public Subscription AddSubscription(Type eventType, Type handlerType)
    {
        var subscription = SubscriptionHelper.CreateFromTypes(eventType, handlerType);
        _subscriptions.Add(subscription);

        return subscription;
    }

    public bool HasSubscriptions(string eventName)
    {
        return _subscriptions.Any(s => s.EventName == eventName);
    }
}
