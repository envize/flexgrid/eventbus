using Microsoft.Extensions.Hosting;

namespace Envize.FlexGrid.EventBus;

public class EventBusService : IHostedService
{
    private readonly IEventBusController _bus;

    public EventBusService(IEventBusController bus)
    {
        _bus = bus;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var loader = new SubscriptionLoader(_bus);
        loader.SubscribeHandlersFromAssembly();

        await _bus.Start();
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        await _bus.Stop();
    }
}
