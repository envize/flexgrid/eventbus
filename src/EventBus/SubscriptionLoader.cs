namespace Envize.FlexGrid.EventBus;

public class SubscriptionLoader
{
    private readonly IEventBusController _bus;

    public SubscriptionLoader(IEventBusController bus)
    {
        _bus = bus;
    }

    public void SubscribeHandlersFromAssembly()
    {
        var handlerTypes = GetEventHandlerTypes();
        var typePairs = PairEventHandlersWithEvents(handlerTypes);

        foreach (var pair in typePairs)
        {
            _bus.Subscribe(pair.EventType, pair.HandlerType);
        }
    }

    public static IEnumerable<Type> GetEventHandlerTypes()
    {
        // todo: unsafe af
        return AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => !type.IsAbstract &&
                           type.IsClass &&
                           type.GetInterfaces().Any(SubscriptionHelper.MatchesHandlerInterface));
    }

    private IEnumerable<Subscription> PairEventHandlersWithEvents(IEnumerable<Type> handlerTypes)
    {
        return handlerTypes.Select(type =>
            SubscriptionHelper.CreateFromTypes(
                SubscriptionHelper.GetEventTypeFromHandlerType(type),
                type));
    }
}
