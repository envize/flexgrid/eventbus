using Microsoft.Extensions.DependencyInjection;

namespace Envize.FlexGrid.EventBus;

public static class SubscriptionLoaderExtensions
{
    public static IServiceCollection RegisterEventHandlers(this IServiceCollection services)
    {
        var handlerTypes = SubscriptionLoader.GetEventHandlerTypes();

        foreach (var handlerType in handlerTypes)
        {
            services.AddTransient(handlerType);
        }

        return services;
    }
}
