namespace Envize.FlexGrid.EventBus
{
    public interface IEventBusController : IEventBus
    {
        Task Start();

        Task Stop();
    }
}
