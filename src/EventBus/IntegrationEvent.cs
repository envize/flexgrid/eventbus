namespace Envize.FlexGrid.EventBus;

public abstract class IntegrationEvent
{
    public abstract string Name { get; }
    public Guid Id { get; }
    public DateTime CreationDate { get; }

    protected IntegrationEvent()
    {
        Id = Guid.NewGuid();
        CreationDate = DateTime.UtcNow;
    }
}
