using RabbitMQ.Client;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal interface IChannel : IDisposable
{
    IModel Value { get; }
}
