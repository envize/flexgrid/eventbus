using Envize.FlexGrid.EventBus.DistributedTransactions;
using Envize.FlexGrid.EventBus.DistributedTransactions.Events;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

// TODO: use interface
public class TransactionNamingService
{
    private const string TransactionKey = "transaction";
    private readonly MessageNamingService _naming;

    public TransactionNamingService(MessageNamingService naming)
    {
        _naming = naming;
    }

    public string GetExchangeName(IMessage message)
    {
        return GetExchangeName(message.TransactionKey, message.State, _naming.GetOperationName(message));
    }

    public string GetExchangeName(string transactionKey, string? state, OperationType operation)
    {
        return GetExchangeName(transactionKey, state, _naming.GetOperationName(operation));
    }

    private string GetExchangeName(string transactionKey, string? state, string operation)
    {
        var stateText = operation != MessageNamingService.Failure ? state : null;
        return $"{TransactionKey}:{transactionKey}:{stateText}:{operation}";
    }

    public string GetQueueName(IMessage message)
    {
        return GetQueueName(message.TransactionKey, message.State, _naming.GetOperationName(message));
    }

    public string GetQueueName(string transactionKey, string? state, OperationType operation)
    {
        return GetQueueName(transactionKey, state, _naming.GetOperationName(operation));
    }

    private string GetQueueName(string transactionKey, string? state, string operation)
    {
        var stateText = operation != MessageNamingService.Failure ? state : null;
        return $"{TransactionKey}:{transactionKey}:{stateText}:{operation}:{MachineInfo.ApplicationName}";
    }
}
