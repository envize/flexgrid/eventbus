using Envize.FlexGrid.Monitoring.Tracing;

namespace Envize.FlexGrid.EventBus.RabbitMQ
{
    public class EventConsumerSpan : ConsumerSpan, IDisposable
    {
        public EventConsumerSpan(EventMetaData metadata)
            : base($"consume '{metadata.EventName}' event", metadata.CorrelationId)
        {
            SetTag("producer-id", metadata.Producer);
            SetTag("event-name", metadata.EventName);
            SetTag("event-id", metadata.MessageId);
        }
    }
}
