using RabbitMQ.Client.Events;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal class EventBus : IEventBusController, IEventBus
{
    private readonly IConnectionStore _connections;
    private readonly EventBusHelper _helper;
    private readonly IIntegrationRoutingManager _routing;

    private readonly SubscriptionStore _subscriptions = new();

    public EventBus(
        IConnectionStore connections,
        EventBusHelper helper,
        IIntegrationRoutingManager routing
    )
    {
        _connections = connections;
        _helper = helper;
        _routing = routing;
    }

    public ValueTask DisposeAsync()
    {
        lock (_connections.PublishingChannel)
        {
            _connections.Dispose();
        }

        return new ValueTask();
    }

    public Task Publish<TEvent>(TEvent @event)
        where TEvent : IntegrationEvent, new()
    {
        using var span = new EventProducerSpan<TEvent>(@event);

        var properties = _connections.PublishingChannel.CreateBasicProperties();
        properties.MessageId = @event.Id.ToString();
        properties.Type = @event.Name;
        properties.CorrelationId = span.CorrelationId;
        properties.AppId = MachineInfo.ApplicationId;

        var eventName = @event.Name;

        _routing.Publish(eventName, @event, properties);

        return Task.CompletedTask;
    }

    public Task Start()
    {
        foreach (var (eventName, _) in _subscriptions.GetSubscriptions())
        {
            _routing.RegisterConsumer(eventName, OnMessageReceived);
        }

        return Task.CompletedTask;
    }

    public Task Stop()
    {
        return Task.CompletedTask;
    }

    public Task Subscribe(Type eventType, Type handlerType)
    {
        var (eventName, _, _) = _subscriptions.AddSubscription(eventType, handlerType);

        _routing.DeclareQueue(eventName);
        // todo: what if it gets called when it's already running? 

        return Task.CompletedTask;
    }

    // todo: wrap and unpack this
    private void OnMessageReceived(object? model, BasicDeliverEventArgs args)
    {
        var buffer = args.Body.ToArray();
        var eventName = args.BasicProperties.Type;

        var @event = _helper.DeserializeEventObject(eventName, buffer);
        var handlerTypes = _subscriptions.GetEventHandlers(eventName);

        Task.Run(async () => // todo: move this to a background queue
        {
            var metadata = new EventMetaData(args.BasicProperties);
            using var span = new EventConsumerSpan(metadata);

            await _helper.InvokeEventHandlers(@event, handlerTypes);
        });
    }
}
