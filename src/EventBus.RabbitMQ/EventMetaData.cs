using Envize.FlexGrid.Monitoring.Tracing;
using RabbitMQ.Client;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

public struct EventMetaData
{
    public string Producer { get; init; }
    public CorrelationId? CorrelationId { get; init; } = default;
    public string EventName { get; set; }
    public string MessageId { get; set; }

    public EventMetaData(IBasicProperties properties)
    {
        Producer = properties.AppId;
        EventName = properties.Type;
        MessageId = properties.MessageId;

        if (properties.CorrelationId is not null)
        {
            CorrelationId = properties.CorrelationId;
        }
    }
}
