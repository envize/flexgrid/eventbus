using Envize.FlexGrid.EventBus.DistributedTransactions;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal class TransactionPublisher : IEventPublisher,
    IEventCompletionPublisher,
    IEventFailurePublisher,
    IRollbackPublisher,
    IRollbackCompletionPublisher
{
    private readonly ITransactionRoutingManager _routing;

    public TransactionPublisher(ITransactionRoutingManager routing)
    {
        _routing = routing;
    }

    public Task Publish(EventMessage message)
    {
        _routing.Publish(message);

        return Task.CompletedTask;
    }

    public Task Publish(EventCompletionMessage message)
    {
        _routing.Publish(message);

        return Task.CompletedTask;
    }

    public Task Publish(EventFailureMessage message)
    {
        _routing.Publish(message);

        return Task.CompletedTask;
    }

    public Task Publish(RollbackMessage message)
    {
        _routing.Publish(message);

        return Task.CompletedTask;
    }

    public Task Publish(RollbackCompletionMessage message)
    {
        _routing.Publish(message);

        return Task.CompletedTask;
    }
}
