using Envize.FlexGrid.EventBus.DistributedTransactions;
using Envize.FlexGrid.EventBus.DistributedTransactions.Events;
using Envize.FlexGrid.Monitoring.Tracing;
using Microsoft.Extensions.Hosting;

namespace Envize.FlexGrid.EventBus.RabbitMQ.HostedServices;

internal class EventCompletionReceiverService : IHostedService
{
    private readonly IRoutingActions _actions;
    private readonly IEventCompletionReceiver _receiver;
    private readonly IEventCompletionMessageDeserializer _deserializer;
    private readonly IEventHookService _hooks;
    private readonly TransactionNamingService _naming;

    public EventCompletionReceiverService(
        IRoutingActions actions,
        IEventCompletionReceiver receiver,
        IEventCompletionMessageDeserializer deserializer,
        IEventHookService hooks,
        TransactionNamingService naming)
    {
        _actions = actions;
        _receiver = receiver;
        _deserializer = deserializer;
        _hooks = hooks;
        _naming = naming;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        var operation = OperationType.EventCompletion;
        var hooks = _hooks.GetMountableHooks(operation);

        foreach (var (transaction, state, _) in hooks)
        {
            var exchange = _naming.GetExchangeName(transaction, state, operation);
            var queue = _naming.GetQueueName(transaction, state, operation);

            _actions.DeclareQueue(exchange, queue, exchange);
            _actions.RegisterConsumerAsync(queue, async (_, args) =>
            {
                var correlationId = args.BasicProperties.CorrelationId;
                using var span = new ConsumerSpan("consume event completion", correlationId);
                span.SetMessagingInfo(args, queue);

                var buffer = args.Body.ToArray();
                var message = await _deserializer.Deserialize(buffer);

                span.SetTransactionMessage(message);

                await _receiver.Receive(message);
            });
        }

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        // throw new NotImplementedException();

        return Task.CompletedTask;
    }
}
