using Envize.FlexGrid.EventBus.DistributedTransactions;
using Envize.FlexGrid.EventBus.DistributedTransactions.Events;
using Envize.FlexGrid.Monitoring.Tracing;
using Microsoft.Extensions.Hosting;

namespace Envize.FlexGrid.EventBus.RabbitMQ.HostedServices;

internal class EventFailureReceiverService : IHostedService
{
    private readonly IRoutingActions _actions;
    private readonly IEventFailureReceiver _receiver;
    private readonly IEventFailureMessageDeserializer _deserializer;
    private readonly IEventHookService _hooks;
    private readonly TransactionNamingService _naming;

    public EventFailureReceiverService(
        IRoutingActions actions,
        IEventFailureReceiver receiver,
        IEventFailureMessageDeserializer deserializer,
        IEventHookService hooks,
        TransactionNamingService naming)
    {
        _actions = actions;
        _receiver = receiver;
        _deserializer = deserializer;
        _hooks = hooks;
        _naming = naming;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        var operation = OperationType.EventFailure;
        var hooks = _hooks.GetMountableHooks(operation);

        foreach (var (transaction, _, _) in hooks)
        {
            var exchange = _naming.GetExchangeName(transaction, null, operation);
            var queue = _naming.GetQueueName(transaction, null, operation);

            _actions.DeclareQueue(exchange, queue, exchange);
            _actions.RegisterConsumerAsync(queue, async (_, args) =>
            {
                var correlationId = args.BasicProperties.CorrelationId;
                using var span = new ConsumerSpan("consume failure", correlationId);
                span.SetMessagingInfo(args, queue);

                var buffer = args.Body.ToArray();
                var message = await _deserializer.Deserialize(buffer);

                span.SetTransactionMessage(message);

                await _receiver.Receive(message);
            });
        }

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        // throw new NotImplementedException();

        return Task.CompletedTask;
    }
}
