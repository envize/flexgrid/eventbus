namespace Envize.FlexGrid.EventBus.RabbitMQ
{
    public class RabbitConfiguration
    {
        public const string SectionName = "RabbitConfiguration";
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? Hostname { get; set; }
        public string VirtualHost { get; set; } = "/";
        public int Port { get; set; } = 5672;
    }
}
