using RabbitMQ.Client;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal interface IConnectionStore : IDisposable
{
    IModel PublishingChannel { get; }
    IModel SubscriptionChannel { get; }
}
