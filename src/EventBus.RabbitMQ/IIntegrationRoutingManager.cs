using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

public interface IIntegrationRoutingManager
{
    void DeclareQueue(string eventName);
    void RegisterConsumer(string eventName, EventHandler<BasicDeliverEventArgs> callback);
    void Publish(string eventName, object body, IBasicProperties properties);
}
