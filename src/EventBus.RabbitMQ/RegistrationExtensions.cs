using Envize.FlexGrid.EventBus.DistributedTransactions;
using Envize.FlexGrid.EventBus.RabbitMQ.HostedServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Envize.FlexGrid.EventBus.RabbitMQ
{
    public static class RegistrationExtensions
    {
        public static IServiceCollection AddRabbitMQEventBus<TAssemblyMarker>(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            var configuration = provider.GetRequiredService<IConfiguration>();

            return AddRabbitMQEventBus<TAssemblyMarker>(services, configuration);
        }

        public static IServiceCollection AddRabbitMQEventBus<TAssemblyMarker>(
            this IServiceCollection services,
            IConfiguration configuration
        )
        {
            services
                .Configure<RabbitConfiguration>(configuration.GetSection(RabbitConfiguration.SectionName))
                .AddTransient<IChannel, Channel>()
                .AddTransient<TransactionNamingService>()
                .AddSingleton<IConnectionStore, ConnectionStore>()
                .AddSingleton<EventBusHelper>()
                .AddSingleton<EventBus>()
                .AddSingleton<IEventBus, EventBus>(x => x.GetRequiredService<EventBus>())
                .AddSingleton<IEventBusController, EventBus>(x => x.GetRequiredService<EventBus>())
                .AddSingleton<SubscriptionStore>()
                .AddSingleton<IRoutingActions, RoutingActions>()
                .AddSingleton<IIntegrationRoutingManager, IntegrationRoutingManager>()
                .AddSingleton<ITransactionRoutingManager, TransactionRoutingManager>()
                .AddHostedService<EventBusService>()
                .AddHostedService<EventReceiverService>()
                .AddHostedService<EventCompletionReceiverService>()
                .AddHostedService<EventFailureReceiverService>()
                .AddHostedService<RollbackReceiverService>()
                .AddHostedService<RollbackCompletionReceiverService>()
                .AddDistributedTransactions<TAssemblyMarker, TransactionPublisher>(configuration)
                .RegisterEventHandlers();

            return services;
        }
    }
}
