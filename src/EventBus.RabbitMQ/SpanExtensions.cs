using Envize.FlexGrid.Monitoring.Tracing;
using RabbitMQ.Client.Events;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

public static class SpanExtensions
{
    const string MessagingKey = "messaging";

    public static Span SetMessagingInfo(this Span span, BasicDeliverEventArgs args, string queue)
    {
        span.SetTag($"{MessagingKey}.exchange", args.Exchange);
        span.SetTag($"{MessagingKey}.routing-key", args.RoutingKey);
        span.SetTag($"{MessagingKey}.queue", queue);

        return span;
    }

    public static Span SetMessagingInfo(this Span span, string exchange, string routingKey)
    {
        const string group = "messaging";

        span.SetTag($"{group}.exchange", exchange);
        span.SetTag($"{group}.routing-key", routingKey);

        return span;
    }
}
