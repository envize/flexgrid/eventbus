using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal interface IRoutingActions
{
    void DeclareExchange(string exchangeName);
    void DeclareQueue(string exchangeName, string queueName, string routingKey);
    void RegisterConsumer(string queueName, EventHandler<BasicDeliverEventArgs> callback);
    void RegisterConsumerAsync(string queueName, Func<object?, BasicDeliverEventArgs, Task> callback);
    void Publish(string exchangeName, string routingKey, object body, IBasicProperties properties);
}
