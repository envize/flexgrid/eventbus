using Envize.FlexGrid.EventBus.DistributedTransactions;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal interface ITransactionRoutingManager
{
    void Publish(IMessage message);
}
