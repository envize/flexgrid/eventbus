using System.Collections.Concurrent;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal class DeclarationQueue
{
    private readonly ConcurrentQueue<string> _queue = new();

    public void Enqueue(string eventName)
    {
        _queue.Enqueue(eventName);
    }

    public bool TryDequeue(out string? eventName)
    {
        return _queue.TryDequeue(out eventName);
    }

    public bool IsEmpty()
    {
        return _queue.IsEmpty;
    }
}
