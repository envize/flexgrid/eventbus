using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal class IntegrationRoutingManager : IIntegrationRoutingManager
{
    private const string IntegrationKey = "integration";
    
    private readonly IRoutingActions _actions;

    public IntegrationRoutingManager(IRoutingActions actions)
    {
        _actions = actions;
    }

    public void DeclareQueue(string eventName)
    {
        var exchangeName = GetExchangeName(eventName);
        var queueName = GetQueueName(eventName);
        var routingKey = GetRoutingKey(eventName);
        
        _actions.DeclareQueue(exchangeName, queueName, routingKey);
    }

    public void RegisterConsumer(string eventName, EventHandler<BasicDeliverEventArgs> callback)
    {
        var queueName = GetQueueName(eventName);
        
        _actions.RegisterConsumer(queueName, callback);
    }

    public void Publish(string eventName, object body, IBasicProperties properties)
    {
        var exchangeName = GetExchangeName(eventName);
        var routingKey = GetRoutingKey(eventName);
        
        _actions.Publish(exchangeName, routingKey, body, properties);
    }

    private string GetExchangeName(string eventName)
    {
        return $"{IntegrationKey}:{eventName}";
    }

    private string GetQueueName(string eventName)
    {
        return $"{IntegrationKey}:{eventName}:{MachineInfo.ApplicationName}";
    }

    private string GetRoutingKey(string eventName)
    {
        return GetExchangeName(eventName);
    }
}
