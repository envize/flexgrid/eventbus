using Envize.FlexGrid.Resilience;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal class Channel : IChannel
{
    private readonly RabbitConfiguration _configuration;
    private readonly ILogger<Channel> _logger;

    private IConnection? _connection;
    private IModel? _channel;

    public IModel Value => GetOrConnect();

    public Channel(IOptions<RabbitConfiguration> configuration, ILogger<Channel> logger)
    {
        _configuration = configuration.Value;
        _logger = logger;
    }

    private void Connect()
    {
        if (_channel is not null)
        {
            throw new InvalidOperationException("Already connected."); // todo: better exception method
        }

        var policy = Policies.RetryUpToTimeout<BrokerUnreachableException>(TimeSpan.FromMinutes(5),
            (attempt, timeToWait) =>
                _logger.LogWarning("Could not connect to the EventBroker, retrying in {delay}.", timeToWait));

        var factory = new ConnectionFactory()
        {
            HostName = _configuration.Hostname,
            UserName = _configuration.Username,
            Password = _configuration.Password,
            VirtualHost = _configuration.VirtualHost,
            Port = _configuration.Port
        };

        _connection = policy.Execute(() => factory.CreateConnection());
        _channel = _connection.CreateModel();
    }

    public void Dispose()
    {
        CloseAndDispose(Value);
        CloseAndDispose(_connection);

        _channel = null;
        _connection = null;
    }

    private IModel GetOrConnect()
    {
        if (_channel is null)
        {
            Connect();
        }

        return _channel!;
    }

    private void CloseAndDispose(IModel? channel)
    {
        if (channel is null)
        {
            return;
        }

        if (channel.IsOpen)
        {
            channel.Close();
        }

        channel.Dispose();
    }

    private void CloseAndDispose(IConnection? connection)
    {
        if (connection is null)
        {
            return;
        }

        if (connection.IsOpen)
        {
            connection.Close();
        }

        connection.Dispose();
    }
}
