using System.Text;
using System.Text.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal class RoutingActions : IRoutingActions
{
    private readonly IConnectionStore _connections;

    public RoutingActions(IConnectionStore connections)
    {
        _connections = connections;
    }

    // todo: cache this
    public void DeclareExchange(string exchangeName)
    {
        _connections.SubscriptionChannel!
            .ExchangeDeclare(exchange: exchangeName,
                type: ExchangeType.Direct,
                durable: true);
    }

    // todo: cache this
    public void DeclareQueue(string exchangeName, string queueName, string routingKey)
    {
        DeclareExchange(exchangeName);

        _connections.SubscriptionChannel!
            .QueueDeclare(queue: queueName,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

        _connections.SubscriptionChannel!
            .QueueBind(queue: queueName,
                exchange: exchangeName,
                routingKey: routingKey);
    }

    public void RegisterConsumer(string queueName, EventHandler<BasicDeliverEventArgs> callback)
    {
        var consumer = new EventingBasicConsumer(_connections.SubscriptionChannel);
        consumer.Received += callback;

        _connections.SubscriptionChannel!
            .BasicConsume(queue: queueName,
                autoAck: true,
                consumer: consumer);
    }

    public void RegisterConsumerAsync(string queueName, Func<object?, BasicDeliverEventArgs, Task> callback)
    {
        RegisterConsumer(queueName, (sender, args) =>
        {
            // TODO: move to background queue
            Task.Run(async () => await callback(sender, args));
        });
    }

    public void Publish(string exchangeName, string routingKey, object body, IBasicProperties properties)
    {
        DeclareExchange(exchangeName);

        var json = JsonSerializer.Serialize(body);
        var buffer = Encoding.UTF8.GetBytes(json);

        lock (_connections.PublishingChannel)
        {
            _connections.PublishingChannel
                .BasicPublish(exchange: exchangeName,
                    routingKey: routingKey,
                    basicProperties: properties,
                    body: buffer);
        }
    }
}
