using Envize.FlexGrid.Monitoring.Tracing;

namespace Envize.FlexGrid.EventBus.RabbitMQ
{
    public class EventProducerSpan<TEvent> : ProducerSpan, IDisposable
        where TEvent : IntegrationEvent, new()
    {
        public EventProducerSpan(TEvent @event)
            : base($"produce '{@event.Name}' event")
        {
            SetTag("producer-id", MachineInfo.ApplicationId);
            SetTag("event-name", @event.Name);
            SetTag("event-id", @event.Id.ToString());
        }
    }
}
