using Envize.FlexGrid.EventBus.DistributedTransactions;
using Envize.FlexGrid.Monitoring.Tracing;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal class TransactionRoutingManager : ITransactionRoutingManager
{
    private readonly IRoutingActions _actions;
    private readonly IConnectionStore _connections;
    private readonly TransactionNamingService _naming;

    public TransactionRoutingManager(
        IRoutingActions actions,
        IConnectionStore connections,
        TransactionNamingService naming
    )
    {
        _actions = actions;
        _connections = connections;
        _naming = naming;
    }

    public void Publish(IMessage message)
    {
        using var span = new ProducerSpan("publish transaction event");
        span.SetTransactionMessage(message);

        var exchange = _naming.GetExchangeName(message);
        var routingKey = _naming.GetExchangeName(message);

        span.SetMessagingInfo(exchange, routingKey);

        _actions.DeclareExchange(exchange);

        var properties = _connections.PublishingChannel.CreateBasicProperties();
        properties.MessageId = message.InstanceId.ToString();
        // properties.Type = GetOperationName(message);
        properties.ContentType = "application/json";
        properties.CorrelationId = span.CorrelationId;
        properties.AppId = MachineInfo.ApplicationId;

        _actions.Publish(exchange, routingKey, message, properties);
    }
}
