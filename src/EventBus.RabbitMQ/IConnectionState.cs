namespace Envize.FlexGrid.EventBus.RabbitMQ;

public interface IConnectionState
{
    bool IsConnected { get; }
    void Connect();
    void Disconnect();
}
