using RabbitMQ.Client;

namespace Envize.FlexGrid.EventBus.RabbitMQ;

internal class ConnectionStore : IConnectionStore
{
    private readonly IChannel _publishingChannel;
    public IModel PublishingChannel => _publishingChannel.Value;

    private readonly IChannel _subscriptionChannel;
    public IModel SubscriptionChannel => _subscriptionChannel.Value;

    public ConnectionStore(IChannel publishingChannel, IChannel subscriptionChannel)
    {
        _publishingChannel = publishingChannel;
        _subscriptionChannel = subscriptionChannel;
    }

    public void Dispose()
    {
        _publishingChannel.Dispose();
        _subscriptionChannel.Dispose();
    }
}
