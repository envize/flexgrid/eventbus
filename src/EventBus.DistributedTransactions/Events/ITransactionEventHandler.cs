namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface ITransactionEventHandler<TPayload, TResult>
{
    Task<TResult> Handle(TPayload payload); // todo: add cancellationtoken
    Task Rollback(TPayload payload, TResult result);
}
