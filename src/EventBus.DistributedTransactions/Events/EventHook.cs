namespace Envize.FlexGrid.EventBus.DistributedTransactions.Events;

public record EventHook(string Transaction, string? State, OperationType Operation);
