using Envize.FlexGrid.EventBus.DistributedTransactions.Events;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventHookService
{
    IEnumerable<EventHook> GetMountableHooks(OperationType operation);
}
