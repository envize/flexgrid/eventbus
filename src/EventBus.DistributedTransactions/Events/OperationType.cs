namespace Envize.FlexGrid.EventBus.DistributedTransactions.Events;

public enum OperationType
{
    Event,
    EventCompletion,
    EventFailure,
    Rollback,
    RollbackCompletion
}
