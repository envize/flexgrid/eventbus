using Envize.FlexGrid.EventBus.DistributedTransactions.DependencyInjection;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Events;

internal class EventHandlerStore : IEventHandlerStore
{
    private readonly IServiceProvider _provider;
    private readonly IDictionary<TransactionState, HandlerDetails> _handlers;

    public EventHandlerStore(IServiceProvider provider, Type assemblyMarker)
    {
        _provider = provider;
        _handlers = new EventHandlerAssemblyScanner()
            .GetHandlers(assemblyMarker)
            .ToDictionary(x => new TransactionState(x.Key, x.State), x => x);
    }

    public bool Exists(string key, string state)
    {
        return _handlers.ContainsKey(new(key, state));
    }

    public object CreateInstance(string key, string state)
    {
        if (!Exists(key, state))
        {
            throw new ArgumentException($"No event handler defined for {key}.{state}.");
        }

        var details = _handlers[new(key, state)];

        return _provider.GetService(details.HandlerType)!;
    }

    public HandlerDetails GetDetails(string key, string state)
    {
        if (!Exists(key, state))
        {
            throw new ArgumentException($"No event handler defined for {key}.{state}.");
        }

        return _handlers[new(key, state)];
    }

    public IEnumerable<(string Key, string State)> GetAll()
    {
        return _handlers.Keys
            .Select(x => (x.Key, x.State));
    }

    private readonly record struct TransactionState
    {
        public string Key { get; init; }
        public string State { get; init; }

        public TransactionState(string key, string state)
        {
            Key = key;
            State = state;
        }

        public override string? ToString()
        {
            return $"{Key}.{State}";
        }
    }
}
