namespace Envize.FlexGrid.EventBus.DistributedTransactions.Events;

internal class EventHookService : IEventHookService
{
    private readonly IEventHandlerStore _store;
    private readonly IEnumerable<TransactionDetails> _details;
    private IEnumerable<EventHook>? _hooks;

    public EventHookService(
        IEventHandlerStore store,
        IEnumerable<ITransactionController> controllers
    )
    {
        _store = store;
        _details = controllers.Select(controller => controller.Define());
    }

    public IEnumerable<EventHook> GetMountableHooks(OperationType operation)
    {
        _hooks ??= GetMountableHooks();
        return _hooks.Where(x => x.Operation == operation);
    }

    private IEnumerable<EventHook> GetMountableHooks()
    {
        foreach (var @event in _store.GetAll())
        {
            yield return new(@event.Key, @event.State, OperationType.Event);
            yield return new(@event.Key, @event.State, OperationType.Rollback);
        }

        foreach (var details in _details)
        {
            yield return new(details.Key, null, OperationType.EventFailure);

            var states = details.Events
                .SelectMany(x => new[] {x.TriggerState, x.ResultState})
                .ToList()
                .Distinct()
                .Where(x => x != details.EndState);

            foreach (var state in states)
            {
                yield return new(details.Key, state, OperationType.EventCompletion);
                yield return new(details.Key, state, OperationType.RollbackCompletion);
            }
        }
    }
}
