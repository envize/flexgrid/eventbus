namespace Envize.FlexGrid.EventBus.DistributedTransactions;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class TransactionEventAttribute : Attribute
{
    public string Key { get; init; }
    public string State { get; init; }

    public TransactionEventAttribute(string key, string state)
    {
        Key = key;
        State = state;
    }
}
