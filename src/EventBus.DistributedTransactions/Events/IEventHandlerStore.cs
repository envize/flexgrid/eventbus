namespace Envize.FlexGrid.EventBus.DistributedTransactions.Events;

internal interface IEventHandlerStore
{
    bool Exists(string key, string state);
    object CreateInstance(string key, string state);
    HandlerDetails GetDetails(string key, string state);
    IEnumerable<(string Key, string State)> GetAll();
}
