using System.Reflection;
using Envize.FlexGrid.Monitoring.Tracing;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Events;

internal class EventRunner : IEventRunner
{
    private readonly IEventHandlerStore _store;

    public EventRunner(IEventHandlerStore store)
    {
        _store = store;
    }

    public async Task<EventCompletionMessage> Invoke(EventMessage message)
    {
        if (!_store.Exists(message.TransactionKey, message.State))
        {
            throw new EventNotDefinedException(message.TransactionKey, message.State);
        }

        var handlerInstance = _store.CreateInstance(message.TransactionKey, message.State);

        var methodHandle = handlerInstance
            .GetType()
            .GetMethod(nameof(ITransactionEventHandler<object, object>.Handle))!;

        // todo: cast JsonElement .GetRawText() .Deserialize()

        object? result = null;

        try
        {
            result = await Task.Run<object>(async () =>
            {
                using var span = new Span("execute event");
                span.SetTransactionMessage(message);

                try
                {
                    dynamic awaitable = methodHandle.Invoke(handlerInstance, new object[] {message.Payload})!;
                    return await awaitable;
                }
                catch
                {
                    span.SetError();
                    // todo: log exception
                    throw;
                }
            });
        }
        catch (TargetInvocationException invocationException)
            when (invocationException.InnerException is not null)
        {
            throw new EventExecutionException(message, invocationException.InnerException);
        } // maybe include a catch-all

        return new(
            instanceId: message.InstanceId,
            transactionKey: message.TransactionKey,
            state: message.State,
            result: result
        );
    }

    public async Task<RollbackCompletionMessage> InvokeRollback(RollbackMessage message)
    {
        if (!_store.Exists(message.TransactionKey, message.State))
        {
            throw new EventNotDefinedException(message.TransactionKey, message.State);
        }

        var handlerInstance = _store.CreateInstance(message.TransactionKey, message.State);

        var methodHandle = handlerInstance
            .GetType()
            .GetMethod(nameof(ITransactionEventHandler<object, object>.Rollback))!;

        try
        {
            await Task.Run(async () =>
            {
                using var span = new Span("execute rollback");
                span.SetTransactionMessage(message);

                try
                {
                    dynamic awaitable =
                        methodHandle.Invoke(handlerInstance, new object[] {message.Payload, message.Result!})!;
                    await awaitable;
                }
                catch
                {
                    span.SetError();
                    // todo: log exception
                    throw;
                }
            });
        }
        catch (TargetInvocationException invocationException)
            when (invocationException.InnerException is not null)
        {
            throw new RollbackExecutionException(message, invocationException.InnerException);
        } // maybe include catch-all

        return new(
            instanceId: message.InstanceId,
            transactionKey: message.TransactionKey,
            state: message.State
        );
    }
}
