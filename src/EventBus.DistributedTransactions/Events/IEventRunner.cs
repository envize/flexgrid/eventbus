namespace Envize.FlexGrid.EventBus.DistributedTransactions.Events;

internal interface IEventRunner
{
    Task<EventCompletionMessage> Invoke(EventMessage message);
    Task<RollbackCompletionMessage> InvokeRollback(RollbackMessage message);
}
