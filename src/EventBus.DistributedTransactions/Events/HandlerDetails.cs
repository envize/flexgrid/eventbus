namespace Envize.FlexGrid.EventBus.DistributedTransactions.Events;

internal readonly record struct HandlerDetails
{
    public string Key { get; init; }
    public string State { get; init; }
    public Type HandlerType { get; init; }
    public Type PayloadType { get; init; }
    public Type ResultType { get; init; }

    public HandlerDetails(
        string key,
        string state,
        Type handlerType,
        Type payloadType,
        Type resultType
    )
    {
        Key = key;
        State = state;
        HandlerType = handlerType;
        PayloadType = payloadType;
        ResultType = resultType;
    }

    public void Deconstruct(out Type payloadType, out Type resultType)
    {
        payloadType = PayloadType;
        resultType = ResultType;
    }
}
