namespace Envize.FlexGrid.EventBus.DistributedTransactions.DependencyInjection;

internal class TransactionControllerAssemblyScanner
{
    public IEnumerable<Type> GetControllers(Type assemplyMarker)
    {
        var controllerType = typeof(ITransactionController);

        return assemplyMarker.Assembly.DefinedTypes
            .Where(type => !type.IsInterface &&
                !type.IsAbstract &&
                type.IsAssignableTo(controllerType));
    }
}
