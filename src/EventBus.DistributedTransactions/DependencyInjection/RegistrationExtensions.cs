using Envize.FlexGrid.EventBus.DistributedTransactions.Configuration;
using Envize.FlexGrid.EventBus.DistributedTransactions.DependencyInjection;
using Envize.FlexGrid.EventBus.DistributedTransactions.Events;
using Envize.FlexGrid.EventBus.DistributedTransactions.Messaging;
using Envize.FlexGrid.EventBus.DistributedTransactions.Transactions;
using Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence;
using Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence.InMemory;
using Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence.Mongo;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public static class RegistrationExtensions
{
    public static IServiceCollection AddDistributedTransactions<TAssemblyMarker, TPublisher>(
        this IServiceCollection services,
        IConfiguration configuration
    )
        where TPublisher : class,
        IEventPublisher,
        IEventCompletionPublisher,
        IEventFailurePublisher,
        IRollbackPublisher,
        IRollbackCompletionPublisher
    {
        return AddDistributedTransactions<TAssemblyMarker, TPublisher, TPublisher, TPublisher, TPublisher, TPublisher>(
            services, configuration);
    }

    public static IServiceCollection AddDistributedTransactions
    <
        TAssemblyMarker,
        TEventPublisher,
        TEventCompletionPublisher,
        TEventFailurePublisher,
        TRollbackPublisher,
        TRollbackCompletionPublisher
    >(
        this IServiceCollection services,
        IConfiguration configuration
    )
        where TEventPublisher : class, IEventPublisher
        where TEventCompletionPublisher : class, IEventCompletionPublisher
        where TEventFailurePublisher : class, IEventFailurePublisher
        where TRollbackPublisher : class, IRollbackPublisher
        where TRollbackCompletionPublisher : class, IRollbackCompletionPublisher
    {
        services
            .Configure<MongoOptions>(configuration.GetSection(MongoOptions.SectionName))
            .AddTransactionControllers<TAssemblyMarker>()
            .AddEventHandlers<TAssemblyMarker>()
            .AddTransient(typeof(Lazy<>), typeof(LazyDi<>))
            .AddTransient<ITransactionRunner, TransactionRunner>()
            .AddTransient<ITransaction, Transaction>()
            .AddTransient<PropertyDeserializer>()
            .AddTransient<MessageNamingService>()
            .AddSingleton<IEventPublisher, TEventPublisher>()
            .AddSingleton<IEventCompletionPublisher, TEventCompletionPublisher>()
            .AddSingleton<IEventFailurePublisher, TEventFailurePublisher>()
            .AddSingleton<IRollbackPublisher, TRollbackPublisher>()
            .AddSingleton<IRollbackCompletionPublisher, TRollbackCompletionPublisher>()
            .AddSingleton<IEventReceiver, EventReceiver>()
            .AddSingleton<IEventCompletionReceiver, EventCompletionReceiver>()
            .AddSingleton<IEventFailureReceiver, EventFailureReceiver>()
            .AddSingleton<IRollbackReceiver, RollbackReceiver>()
            .AddSingleton<IRollbackCompletionReceiver, RollbackCompletionReceiver>()
            .AddSingleton<ITransactionStore, TransactionStore>()
            .AddSingleton<IEventRunner, EventRunner>()
            .AddSingleton<IEventHandlerStore, EventHandlerStore>(
                provider => new EventHandlerStore(provider, typeof(TAssemblyMarker)))
            .AddSingleton<IEventMessageDeserializer, EventMessageDeserializer>()
            .AddSingleton<IEventCompletionMessageDeserializer, EventCompletionMessageDeserializer>()
            .AddSingleton<IEventFailureMessageDeserializer, EventFailureMessageDeserializer>()
            .AddSingleton<IRollbackMessageDeserializer, RollbackMessageDeserializer>()
            .AddSingleton<IRollbackCompletionMessageDeserializer, RollbackCompletionMessageDeserializer>()
            .AddSingleton<IEventHookService, EventHookService>();

        var options = new TransactionOptions();
        configuration.Bind(TransactionOptions.SectionName, options);

        switch (options.SnapshotStore)
        {
            case SnapshotStore.InMemory:
                services.AddSingleton<ISnapshotStore, InMemorySnapshotStore>();
                break;
            case SnapshotStore.Mongo:
            default:
                services
                    .AddTransient<ISnapshotStore, MongoSnapshotStore>()
                    .AddSingleton<IMongoClient>(provider => new MongoClient(
                        provider // todo: this can create a conflict with an existing mongo client 
                            .GetRequiredService<IOptions<MongoOptions>>()
                            .Value
                            .ConnectionString));

                // todo: create mongo convention, see https://kevsoft.net/2020/06/25/storing-guids-as-strings-in-mongodb-with-csharp.html

                break;
        }

        return services;
    }

    private static IServiceCollection AddEventHandlers<TAssemblyMarker>(this IServiceCollection services)
    {
        var scanner = new EventHandlerAssemblyScanner();
        var handlerTypes = scanner
            .GetHandlers(typeof(TAssemblyMarker))
            .Select(handler => handler.HandlerType);

        foreach (var handlerType in handlerTypes)
        {
            services.AddTransient(handlerType);
        }

        return services;
    }

    private static IServiceCollection AddTransactionControllers<TAssemblyMarker>(this IServiceCollection services)
    {
        var scanner = new TransactionControllerAssemblyScanner();
        var handlerTypes = scanner.GetControllers(typeof(TAssemblyMarker));
        var controllerType = typeof(ITransactionController);

        foreach (var handlerType in handlerTypes)
        {
            services.AddTransient(controllerType, handlerType);
        }

        return services;
    }
}
