using Microsoft.Extensions.DependencyInjection;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.DependencyInjection;

internal class LazyDi<T> : Lazy<T> where T : class
{
    public LazyDi(IServiceProvider provider)
        : base(() => provider.GetRequiredService<T>()) { }
}
