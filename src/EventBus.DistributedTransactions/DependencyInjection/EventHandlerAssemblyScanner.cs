using Envize.FlexGrid.EventBus.DistributedTransactions.Events;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.DependencyInjection;

internal class EventHandlerAssemblyScanner
{
    public IEnumerable<HandlerDetails> GetHandlers(Type assemplyMarker)
    {
        var handlerType = typeof(ITransactionEventHandler<,>);
        var types = assemplyMarker.Assembly.DefinedTypes;

        foreach (var potentialHandlerType in types)
        {
            if (potentialHandlerType.IsAbstract ||
                potentialHandlerType.IsInterface)
            {
                continue;
            }

            var eventAttributes = potentialHandlerType
                .GetCustomAttributes(typeof(TransactionEventAttribute), false)
                .Cast<TransactionEventAttribute>();

            if (!eventAttributes.Any())
            {
                continue;
            }

            // todo: this here should be tweaked big time, abstract class?, interface?, interface of interface?
            // var baseType = potentialHandlerType.BaseType;
            var baseType = potentialHandlerType
                .GetInterfaces()
                .Where(@interface => @interface.IsGenericType)
                .Single(@interface => @interface
                    .GetGenericTypeDefinition()
                    .IsAssignableTo(handlerType));
                // .Select(@interface => @interface.GetGenericTypeDefinition())
                // .Single(@interface => @interface.IsAssignableTo(handlerType)); // todo: WRONG!

            var genericArguments = baseType.GetGenericArguments();
            var payloadType = genericArguments[0];
            var resultType = genericArguments[1];

            foreach (var attribute in eventAttributes)
            {
                yield return new(
                    key: attribute.Key, 
                    state: attribute.State, 
                    handlerType: potentialHandlerType, 
                    payloadType: payloadType, 
                    resultType: resultType
                );
            }
        }
    }
}
