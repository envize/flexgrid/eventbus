namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public readonly record struct RollbackMessage : IMessage
{
    public Guid InstanceId { get; init; }
    public string TransactionKey { get; init; }
    public string State { get; init; }
    public object Payload { get; init; }
    public object Result { get; init; }

    internal RollbackMessage(
        Guid instanceId,
        string transactionKey,
        string state,
        object payload,
        object result
    )
    {
        InstanceId = instanceId;
        TransactionKey = transactionKey;
        State = state;
        Payload = payload;
        Result = result;
    }

    public override string? ToString()
    {
        return $"{TransactionKey}.{State}@rollback {InstanceId.ToString("N")[^5..]}";
    }
}
