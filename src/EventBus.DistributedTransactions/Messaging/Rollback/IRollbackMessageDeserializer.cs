namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IRollbackMessageDeserializer
{
    Task<RollbackMessage> Deserialize(byte[] bytes);
}
