namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IRollbackPublisher
{
    Task Publish(RollbackMessage message);
}
