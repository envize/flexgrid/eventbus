using System.Text.Json;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class RollbackMessageDeserializer : IRollbackMessageDeserializer
{
    private readonly PropertyDeserializer _propertyDeserializer;

    public RollbackMessageDeserializer(PropertyDeserializer propertyDeserializer)
    {
        _propertyDeserializer = propertyDeserializer;
    }

    public async Task<RollbackMessage> Deserialize(byte[] bytes)
    {
        await using var stream = new MemoryStream(bytes);
        var message = await JsonSerializer.DeserializeAsync<RollbackMessage>(stream);

        return message with
        {
            Payload = _propertyDeserializer.DeserializePayload(message, message.Payload),
            Result = _propertyDeserializer.DeserializeResult(message, message.Result)
        };
    }
}
