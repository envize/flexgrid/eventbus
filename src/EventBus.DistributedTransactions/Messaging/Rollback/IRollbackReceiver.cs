namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IRollbackReceiver
{
    Task Receive(RollbackMessage message);
}
