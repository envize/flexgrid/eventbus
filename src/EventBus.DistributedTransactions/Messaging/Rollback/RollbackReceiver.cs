using Envize.FlexGrid.EventBus.DistributedTransactions.Events;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class RollbackReceiver : IRollbackReceiver
{
    private readonly Lazy<IEventRunner> _eventRunner;
    private readonly Lazy<IRollbackCompletionPublisher> _completionPublisher;

    public RollbackReceiver(
        Lazy<IEventRunner> eventRunner,
        Lazy<IRollbackCompletionPublisher> completionPublisher
    )
    {
        _eventRunner = eventRunner;
        _completionPublisher = completionPublisher;
    }
    
    public async Task Receive(RollbackMessage message)
    {
        // todo: this must be done more safely
        var completionMessage = await _eventRunner.Value.InvokeRollback(message);
        await _completionPublisher.Value.Publish(completionMessage);
    }
}
