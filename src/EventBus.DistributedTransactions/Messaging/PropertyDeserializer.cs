using System.Dynamic;
using System.Text.Json;
using Envize.FlexGrid.EventBus.DistributedTransactions.Events;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class PropertyDeserializer
{
    private readonly IEventHandlerStore _store;

    // todo: add cache
    public PropertyDeserializer(IEventHandlerStore store)
    {
        _store = store;
    }

    public object DeserializePayload(IMessage message, object obj)
    {
        if (obj is not JsonElement result)
        {
            return obj;
        }

        Type? type = default;

        if (_store.Exists(message.TransactionKey, message.State))
        {
            var (payloadType, _) = _store.GetDetails(message.TransactionKey, message.State);
            type = payloadType;
        }

        return Deserialize(result, type);
    }

    public object DeserializeResult(IMessage message, object obj)
    {
        if (obj is not JsonElement result)
        {
            return obj;
        }

        Type? type = default;

        if (_store.Exists(message.TransactionKey, message.State))
        {
            var (_, resultType) = _store.GetDetails(message.TransactionKey, message.State);
            type = resultType;
        }

        return Deserialize(result, type);
    }

    private object Deserialize(JsonElement obj, Type? type)
    {
        if (type is not null)
        {
            return obj.Deserialize(type)!;
        }

        var json = JsonSerializer.Serialize(new {obj = obj});

        // todo: try to find an alternative to Newtonsoft
        dynamic wrapper = JsonConvert.DeserializeObject<ExpandoObject>(json);
        return wrapper.obj;
    }
}
