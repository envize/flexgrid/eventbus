namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IMessage
{
    Guid InstanceId { get; }
    string TransactionKey { get; }
    string State { get; }
}
