namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public readonly record struct EventCompletionMessage : IMessage
{
    public Guid InstanceId { get; init; }
    public string TransactionKey { get; init; }
    public string State { get; init; }
    public object Result { get; init; }

    internal EventCompletionMessage(
        Guid instanceId,
        string transactionKey,
        string state,
        object result
    )
    {
        InstanceId = instanceId;
        TransactionKey = transactionKey;
        State = state;
        Result = result;
    }

    public override string? ToString()
    {
        return $"{TransactionKey}.{State}@completion {InstanceId.ToString("N")[^5..]}";
    }
}
