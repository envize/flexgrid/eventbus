namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventCompletionPublisher
{
    Task Publish(EventCompletionMessage message);
}
