using Envize.FlexGrid.EventBus.DistributedTransactions.Transactions;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Messaging;

internal class EventCompletionReceiver : IEventCompletionReceiver
{
    private readonly ITransactionStore _store;

    public EventCompletionReceiver(ITransactionStore store)
    {
        _store = store;
    }

    public async Task Receive(EventCompletionMessage message)
    {
        if (!_store.Exists(message.TransactionKey))
        {
            throw new TransactionNotDefinedException(message.TransactionKey);
        }

        var transaction = _store.Get(message.TransactionKey);
        await transaction.OnCompletion(message);
    }
}
