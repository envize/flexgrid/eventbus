namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventCompletionMessageDeserializer
{
    Task<EventCompletionMessage> Deserialize(byte[] bytes);
}
