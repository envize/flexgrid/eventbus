using System.Text.Json;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class EventCompletionMessageDeserializer : IEventCompletionMessageDeserializer
{
    private readonly PropertyDeserializer _propertyDeserializer;

    public EventCompletionMessageDeserializer(PropertyDeserializer propertyDeserializer)
    {
        _propertyDeserializer = propertyDeserializer;
    }

    public async Task<EventCompletionMessage> Deserialize(byte[] bytes)
    {
        await using var stream = new MemoryStream(bytes);
        var message = await JsonSerializer.DeserializeAsync<EventCompletionMessage>(stream);

        return message with
        {
            Result = _propertyDeserializer.DeserializeResult(message, message.Result)
        };
    }
}
