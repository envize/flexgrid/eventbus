namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventCompletionReceiver
{
    Task Receive(EventCompletionMessage message);
}
