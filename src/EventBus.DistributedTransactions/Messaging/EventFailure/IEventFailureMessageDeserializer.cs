namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventFailureMessageDeserializer
{
    Task<EventFailureMessage> Deserialize(byte[] bytes);
}
