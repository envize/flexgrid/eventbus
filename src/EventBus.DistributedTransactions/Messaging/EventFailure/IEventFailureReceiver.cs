namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventFailureReceiver
{
    Task Receive(EventFailureMessage message);
}
