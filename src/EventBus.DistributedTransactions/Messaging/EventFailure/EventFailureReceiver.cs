using Envize.FlexGrid.EventBus.DistributedTransactions.Transactions;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Messaging;

internal class EventFailureReceiver : IEventFailureReceiver
{
    private readonly ITransactionStore _store;

    public EventFailureReceiver(ITransactionStore store)
    {
        _store = store;
    }

    public async Task Receive(EventFailureMessage message)
    {
        if (!_store.Exists(message.TransactionKey))
        {
            throw new TransactionNotDefinedException(message.TransactionKey);
        }

        var transaction = _store.Get(message.TransactionKey);
        await transaction.OnFailure(message);
    }
}
