namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public readonly record struct EventFailureMessage : IMessage
{
    public Guid InstanceId { get; init; }
    public string TransactionKey { get; init; }
    public string State { get; init; }

    internal EventFailureMessage(
        Guid instanceId,
        string transactionKey,
        string state
    )
    {
        InstanceId = instanceId;
        TransactionKey = transactionKey;
        State = state;
    }

    public override string? ToString()
    {
        return $"{TransactionKey}.{State}@failure {InstanceId.ToString("N")[^5..]}";
    }
}
