namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventFailurePublisher
{
    Task Publish(EventFailureMessage message);
}
