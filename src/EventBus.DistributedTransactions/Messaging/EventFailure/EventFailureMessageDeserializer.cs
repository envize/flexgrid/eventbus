using System.Text.Json;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class EventFailureMessageDeserializer : IEventFailureMessageDeserializer
{
    public async Task<EventFailureMessage> Deserialize(byte[] bytes)
    {
        await using var stream = new MemoryStream(bytes);
        var message = await JsonSerializer.DeserializeAsync<EventFailureMessage>(stream);

        return message;
    }
}
