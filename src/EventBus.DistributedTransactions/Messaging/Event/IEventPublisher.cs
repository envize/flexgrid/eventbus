namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventPublisher
{
    Task Publish(EventMessage message);
}
