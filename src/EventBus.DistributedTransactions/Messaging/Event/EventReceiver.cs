using Envize.FlexGrid.EventBus.DistributedTransactions.Events;
using Microsoft.Extensions.Logging;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class EventReceiver : IEventReceiver
{
    private readonly ILogger<EventReceiver> _logger;
    private readonly Lazy<IEventRunner> _eventRunner;
    private readonly Lazy<IEventCompletionPublisher> _completionPublisher;
    private readonly Lazy<IEventFailurePublisher> _rollbackPublisher;

    public EventReceiver(
        ILogger<EventReceiver> logger,
        Lazy<IEventRunner> eventRunner,
        Lazy<IEventCompletionPublisher> completionPublisher,
        Lazy<IEventFailurePublisher> rollbackPublisher)
    {
        _logger = logger;
        _eventRunner = eventRunner;
        _completionPublisher = completionPublisher;
        _rollbackPublisher = rollbackPublisher;
    }

    public async Task Receive(EventMessage message)
    {
        // todo: check if it has the event handler
        EventCompletionMessage? completionMessage = null;

        try
        {
            completionMessage = await _eventRunner.Value.Invoke(message);
        }
        catch (EventExecutionException exception)
        {
            var failureMessage = new EventFailureMessage(
                instanceId: message.InstanceId,
                transactionKey: message.TransactionKey,
                state: message.State
            );

            _logger.LogError(exception, "event {transactionKey}.{state} with id {instanceId} has failed",
                message.TransactionKey, message.State, message.InstanceId);

            await _rollbackPublisher.Value.Publish(failureMessage);
        }

        await _completionPublisher.Value.Publish(completionMessage!.Value);
    }
}
