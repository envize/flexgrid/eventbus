using System.Text.Json;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class EventMessageDeserializer : IEventMessageDeserializer
{
    private readonly PropertyDeserializer _propertyDeserializer;

    public EventMessageDeserializer(PropertyDeserializer propertyDeserializer)
    {
        _propertyDeserializer = propertyDeserializer;
    }

    public async Task<EventMessage> Deserialize(byte[] bytes)
    {
        await using var stream = new MemoryStream(bytes);
        var message = await JsonSerializer.DeserializeAsync<EventMessage>(stream);

        return message with
        {
            Payload = _propertyDeserializer.DeserializePayload(message, message.Payload)
        };
    }
}
