namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public readonly record struct EventMessage : IMessage
{
    public Guid InstanceId { get; init; }
    public string TransactionKey { get; init; }
    public string State { get; init; }
    public object Payload { get; init; }

    internal EventMessage(
        Guid instanceId,
        string transactionKey,
        string state,
        object payload
    )
    {
        InstanceId = instanceId;
        TransactionKey = transactionKey;
        State = state;
        Payload = payload;
    }

    public override string? ToString()
    {
        return $"{TransactionKey}.{State} {InstanceId.ToString("N")[^5..]}";
    }
}
