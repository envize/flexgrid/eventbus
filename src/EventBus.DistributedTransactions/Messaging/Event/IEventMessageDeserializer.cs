namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventMessageDeserializer
{
    Task<EventMessage> Deserialize(byte[] bytes);
}
