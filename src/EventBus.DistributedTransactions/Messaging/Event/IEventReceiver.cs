namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IEventReceiver
{
    Task Receive(EventMessage message);
}
