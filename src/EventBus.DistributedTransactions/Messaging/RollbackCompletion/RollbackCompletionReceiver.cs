using Envize.FlexGrid.EventBus.DistributedTransactions.Transactions;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class RollbackCompletionReceiver : IRollbackCompletionReceiver
{
    private readonly ITransactionStore _store;

    public RollbackCompletionReceiver(ITransactionStore store)
    {
        _store = store;
    }

    public async Task Receive(RollbackCompletionMessage message)
    {
        if (!_store.Exists(message.TransactionKey))
        {
            throw new TransactionNotDefinedException(message.TransactionKey);
        }

        var transaction = _store.Get(message.TransactionKey);
        await transaction.OnRollbackCompletion(message);
    }
}
