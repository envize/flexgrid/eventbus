namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IRollbackCompletionReceiver
{
    Task Receive(RollbackCompletionMessage message);
}
