namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IRollbackCompletionPublisher
{
    Task Publish(RollbackCompletionMessage message);
}
