using System.Text.Json;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class RollbackCompletionMessageDeserializer : IRollbackCompletionMessageDeserializer
{
    public async Task<RollbackCompletionMessage> Deserialize(byte[] bytes)
    {
        await using var stream = new MemoryStream(bytes);
        var message = await JsonSerializer.DeserializeAsync<RollbackCompletionMessage>(stream);

        return message;
    }
}
