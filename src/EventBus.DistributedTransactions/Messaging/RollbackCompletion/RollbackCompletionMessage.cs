namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public readonly record struct RollbackCompletionMessage : IMessage
{
    public Guid InstanceId { get; init; }
    public string TransactionKey { get; init; }
    public string State { get; init; }

    internal RollbackCompletionMessage(
        Guid instanceId,
        string transactionKey,
        string state
    )
    {
        InstanceId = instanceId;
        TransactionKey = transactionKey;
        State = state;
    }

    public override string? ToString()
    {
        return $"{TransactionKey}.{State}@rollback-completion {InstanceId.ToString("N")[^5..]}";
    }
}
