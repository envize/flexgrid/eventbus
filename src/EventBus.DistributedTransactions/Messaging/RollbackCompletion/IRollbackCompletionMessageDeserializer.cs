namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface IRollbackCompletionMessageDeserializer
{
    Task<RollbackCompletionMessage> Deserialize(byte[] bytes);
}
