namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public class TransactionNotDefinedException : Exception
{
    public TransactionNotDefinedException(string key)
        : base($"Transaction with key '{key}' has not been defined.") { }
}
