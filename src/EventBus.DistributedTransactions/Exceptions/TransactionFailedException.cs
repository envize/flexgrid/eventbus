namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public class TransactionFailedException : Exception
{
    public TransactionFailedException(string key)
        : base($"Transaction '{key}' could not be completed succesfully, and has been rolled back.") { }
}
