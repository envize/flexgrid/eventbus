namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class EventExecutionException : Exception
{
    public EventMessage EventMessage { get; init; }
    
    public EventExecutionException(EventMessage message, Exception innerException)
        : base($"Execution of event '{message.State}' has failed.", innerException)
    {
        EventMessage = message;
    }
}
