namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public class EventNotDefinedException : Exception
{
    public EventNotDefinedException(string key, string state)
        : base($"Transaction event '{key}.{state}' has not been defined.") { }
}
