namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal class RollbackExecutionException : Exception
{
    public RollbackMessage RollbackMessage { get; init; }
    
    public RollbackExecutionException(RollbackMessage message, Exception innerException)
        : base($"Execution of rollback '{message.State}' has failed.", innerException)
    {
        RollbackMessage = message;
    }
}
