namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public enum SnapshotStore
{
    Default = 0,
    InMemory = 1,
    Mongo = 2
}
