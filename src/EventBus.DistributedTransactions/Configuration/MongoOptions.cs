namespace Envize.FlexGrid.EventBus.DistributedTransactions.Configuration;

internal class MongoOptions
{
    public const string SectionName = "EventBus:Transactions:Mongo";

    public string? ConnectionString { get; set; }
    public string? DatabaseName { get; set; }
}
