namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public sealed class TransactionOptions
{
    public const string SectionName = "EventBus:Transactions";
    public SnapshotStore SnapshotStore { get; set; } = SnapshotStore.Default;
}
