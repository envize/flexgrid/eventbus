using Envize.FlexGrid.EventBus.DistributedTransactions.Events;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public class MessageNamingService
{
    public const string Event = "event";
    public const string EventCompletion = "event-completion";
    public const string Failure = "failure";
    public const string Rollback = "rollback";
    public const string RollbackCompletion = "rollback-completion";

    public string GetOperationName(IMessage message) => message switch
    {
        EventMessage => Event,
        EventCompletionMessage => EventCompletion,
        EventFailureMessage => Failure,
        RollbackMessage => Rollback,
        RollbackCompletionMessage => RollbackCompletion,
        _ => throw new InvalidOperationException("Operation is not valid.") // todo: improve exception
    };

    public string GetOperationName(OperationType operation) => operation switch
    {
        OperationType.Event => Event,
        OperationType.EventCompletion => EventCompletion,
        OperationType.EventFailure => Failure,
        OperationType.Rollback => Rollback,
        OperationType.RollbackCompletion => RollbackCompletion,
        _ => throw new InvalidOperationException("Operation is not valid.") // todo: improve exception
    };
}
