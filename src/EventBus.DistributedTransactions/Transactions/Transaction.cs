using System.Collections.Concurrent;
using Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions;

internal partial class Transaction : ITransaction
{
    private readonly ISnapshotStore _snapshots;
    private readonly Lazy<IEventPublisher> _publisher;
    private readonly Lazy<IRollbackPublisher> _rollbackNotifier;
    private readonly IDictionary<Guid, TaskCompletionSource<object>> _handles;
    private TransactionDetails _details;

    public Transaction(
        ISnapshotStore snapshots,
        Lazy<IEventPublisher> publisher,
        Lazy<IRollbackPublisher> rollbackNotifier
    )
    {
        _snapshots = snapshots;
        _publisher = publisher;
        _rollbackNotifier = rollbackNotifier;
        _handles = new ConcurrentDictionary<Guid, TaskCompletionSource<object>>();
    }

    private async Task Fire(Context context, string state, object payload)
    {
        var snapshot = await _snapshots.Create(context, state, payload);
        await _publisher.Value.Publish((EventMessage) snapshot);
    }

    public async Task OnCompletion(EventCompletionMessage message)
    {
        await _snapshots.SetResult(message.InstanceId, message.Result);

        var nextState = GetNextState(message.State);
        if (IsEndState(nextState))
        {
            if (_handles.TryGetValue(message.InstanceId, out var tcs))
            {
                tcs.SetResult(message.Result);
            }

            return;
        }

        var context = new Context(message.TransactionKey, message.InstanceId);
        await Fire(context, nextState, message.Result);
    }

    public async Task<TResult> Run<TResult>(object payload)
    {
        var tcs = new TaskCompletionSource<object>();
        var context = Context.Create(_details.Key);

        _handles.Add(context.InstanceId, tcs);

        await Fire(context, _details.InitialState, payload);

        var result = await tcs.Task;

        _handles.Remove(context.InstanceId);

        if (result is not TResult castedResult)
        {
            throw new InvalidCastException($"Cannot cast result as '{typeof(TResult).Name}'.");
        }

        return castedResult;
    }

    public async Task FireAndForget(object payload)
    {
        var context = Context.Create(_details.Key);
        await Fire(context, _details.InitialState, payload);
    }

    public async Task Init(TransactionDetails details)
    {
        _details = details;

        await _snapshots.Init(details.Key);
    }

    /// <summary>
    /// Rollback transaction on failure.
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public async Task OnFailure(EventFailureMessage message)
    {
        // this is not DRY by design, by moving the removal of a snapshot
        // and the rollback event decision into the same method with the rollback logic,
        // it can become hard to predict what the method actually does.
        await _snapshots.Remove(message.InstanceId, message.State);

        var state = GetPreviousState(message.State);
        await RollbackEvent(message.InstanceId, state);
    }

    public async Task OnRollbackCompletion(RollbackCompletionMessage message)
    {
        // this is not DRY by design, by moving the removal of a snapshot
        // and the rollback event decision into the same method with the rollback logic,
        // it can become hard to predict what the method actually does.
        await _snapshots.Remove(message.InstanceId, message.State);

        if (IsStartState(message.State))
        {
            if (_handles.TryGetValue(message.InstanceId, out var tcs))
            {
                var exception = new TransactionFailedException(_details.Key);
                tcs.SetException(exception);
            }

            return;
        }

        var state = GetPreviousState(message.State);
        await RollbackEvent(message.InstanceId, state);
    }

    private async Task RollbackEvent(Guid id, string state)
    {
        var snapshot = await _snapshots.Get(id);

        // todo: this only calls one, and never checks if is first event
        var message = new RollbackMessage(
            instanceId: id,
            transactionKey: _details.Key,
            state: state,
            payload: snapshot.Payload,
            result: snapshot.Result!
        );

        await _rollbackNotifier.Value.Publish(message);
    }
}
