namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public readonly record struct TransactionDetails
{
    public string Key { get; init; }
    public string InitialState { get; init; }
    public string EndState { get; init; }
    public IEnumerable<Event> Events { get; init; }

    public TransactionDetails(
        string key,
        string initialState,
        string endState,
        IEnumerable<Event> events
    )
    {
        TransactionValidator.ValidateTransactionKey(key, nameof(key));
        TransactionValidator.ValidateState(initialState, nameof(initialState));
        TransactionValidator.ValidateState(endState, nameof(endState));
        TransactionValidator.ValidateEvents(events, nameof(events), initialState, endState);

        Key = key;
        InitialState = initialState;
        EndState = endState;
        Events = events;
    }

    public readonly record struct Event
    {
        public string TriggerState { get; init; }
        public string ResultState { get; init; }

        public Event(string triggerState, string resultState)
        {
            TransactionValidator.ValidateState(triggerState, nameof(triggerState));
            TransactionValidator.ValidateState(resultState, nameof(resultState));

            TriggerState = triggerState;
            ResultState = resultState;
        }
    }
}
