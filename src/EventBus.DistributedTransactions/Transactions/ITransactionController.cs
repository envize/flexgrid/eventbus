﻿namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface ITransactionController
{
    TransactionDetails Define();
}
