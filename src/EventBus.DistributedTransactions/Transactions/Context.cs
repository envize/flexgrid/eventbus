namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal readonly record struct Context
{
    public string Key { get; init; }
    public Guid InstanceId { get; init; }

    public Context(string key)
    {
        Key = key;
        InstanceId = Guid.NewGuid();
    }

    public Context(string key, Guid instanceId)
    {
        Key = key;
        InstanceId = instanceId;
    }

    public static Context Create(string key)
    {
        return new(key, Guid.NewGuid());
    }
}
