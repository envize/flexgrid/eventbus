namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions;

internal class TransactionRunner : ITransactionRunner
{
    private readonly Lazy<ITransactionStore> _lazyStore;
    private ITransactionStore _store => _lazyStore.Value;

    public TransactionRunner(Lazy<ITransactionStore> store)
    {
        _lazyStore = store;
    }

    public async Task<TResult> Run<TResult>(string transactionKey, object payload)
    {
        if (!_store.Exists(transactionKey))
        {
            throw new TransactionNotDefinedException(transactionKey);
        }

        var transaction = _store.Get(transactionKey);
        return await transaction.Run<TResult>(payload);
    }

    public async Task<object> Run(string transactionKey, object payload)
    {
        return await Run<object>(transactionKey, payload);
    }

    public async Task FireAndForget(string transactionKey, object payload)
    {
        var transaction = _store.Get(transactionKey);
        await transaction.FireAndForget(payload);
    }
}
