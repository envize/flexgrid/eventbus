namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions;

internal interface ITransactionStore
{
    bool Exists(string key);
    ITransaction Get(string key);
    bool TryGet(string key, out ITransaction? transaction);
}
