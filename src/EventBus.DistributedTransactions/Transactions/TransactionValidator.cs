using System.Text.RegularExpressions;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal static class TransactionValidator
{
    private const string _pattern = @"^[a-z0-9_-]+$";
    private static readonly Regex Pattern = new(
        _pattern,
        RegexOptions.Compiled | RegexOptions.IgnoreCase
    );

    public static bool IsTransactionKeyValid(string key)
    {
        return Pattern.IsMatch(key);
    }

    public static bool IsStateValid(string state)
    {
        return Pattern.IsMatch(state);
    }

    public static void ValidateTransactionKey(string key, string paramName)
    {
        if (key is null)
        {
            throw new ArgumentNullException(paramName, "The transaction key cannot be null.");
        }
        else if (!IsTransactionKeyValid(key))
        {
            throw new ArgumentException($"The transaction key should be alphanumeric, with the '-' and '_' characters optionally. ({_pattern})", paramName);
        }
    }

    public static void ValidateState(string state, string paramName)
    {
        if (state is null)
        {
            throw new ArgumentNullException(paramName, "State cannot be null.");
        }
        else if (!IsStateValid(state))
        {
            throw new ArgumentException($"State should be alphanumeric, with the '-' and '_' characters optionally. ({_pattern})", paramName);
        }
    }

    public static void ValidateEvents(IEnumerable<TransactionDetails.Event> events, string paramName, string initialState, string endState)
    {
        if (events is null)
        {
            throw new ArgumentNullException(paramName, "Events cannot be null.");
        }
        else if (!events.Any(x => x.TriggerState == initialState))
        {
            throw new ArgumentException($"No event with trigger for the initial state '{initialState}' provided.", paramName);
        }
        else if (!events.Any(x => x.ResultState == endState))
        {
            throw new ArgumentException($"No event that results in the end state '{endState}' provided.", paramName);
        }
        else if (events
            .GroupBy(x => x.TriggerState)
            .Any(group => group.Count() > 1))
        {
            var triggerState = events
                .GroupBy(x => x.TriggerState)
                .First(group => group.Count() > 1)
                .First().TriggerState;

            throw new ArgumentException($"Multiple events registered with the same trigger state '{triggerState}'.", paramName);
        }
        else if (events
            .GroupBy(x => x.ResultState)
            .Any(group => group.Count() > 1))
        {
            var resultState = events
                .GroupBy(x => x.ResultState)
                .First(group => group.Count() > 1)
                .First().ResultState;

            throw new ArgumentException($"Multiple events registered with the same result state '{resultState}'.", paramName);
        }
        else if (events.Any(x => x.TriggerState != initialState &&
            !events.Any(y => x.TriggerState == y.ResultState)))
        {
            var triggerState = events.First(x => x.TriggerState != initialState &&
                !events.Any(y => x.TriggerState == y.ResultState))
                .TriggerState;

            throw new ArgumentException($"Event with trigger state '{triggerState}' cannot be invoked, because no event results in '{triggerState}'.", paramName);
        }
    }
}
