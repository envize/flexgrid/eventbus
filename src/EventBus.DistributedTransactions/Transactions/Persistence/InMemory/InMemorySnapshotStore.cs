using System.Collections.Concurrent;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence.InMemory;

internal class InMemorySnapshotStore : ISnapshotStore
{
    private bool _isInitialized = false;
    private string _transactionKey = default!;
    private IDictionary<Guid, ConcurrentStack<Snapshot>> _snapshotStacks = new ConcurrentDictionary<Guid, ConcurrentStack<Snapshot>>();

    public Task Init(string transactionKey)
    {
        _transactionKey = transactionKey;
        _snapshotStacks = new ConcurrentDictionary<Guid, ConcurrentStack<Snapshot>>();
        _isInitialized = true;

        return Task.CompletedTask;
    }

    public Task<bool> Exists(Guid id)
    {
        GuardAgainstCallBeforeInitialization();

        var exists = _snapshotStacks.ContainsKey(id);
        return Task.FromResult(exists);
    }

    public async Task<Snapshot> Get(Guid id)
    {
        GuardAgainstCallBeforeInitialization();
        
        if (!await Exists(id))
        {
            throw new ArgumentException("No snapshot with given id is present.");
        }

        var stack = GetStack(id);
        var hasPeeked = stack.TryPeek(out var snapshot);

        if (!hasPeeked)
        {
            throw new Exception("Absolute panic! There is a concurrency exception in the snapshot stack!");
        }

        return snapshot;
    }

    public Task<Snapshot> Create(Context context, string state, object payload)
    {
        GuardAgainstCallBeforeInitialization();
        
        if (SnapshotExists(context.InstanceId, state))
        {
            throw new ArgumentException("Snapshot does already exist.");
        }

        var stack = GetOrCreateStack(context.InstanceId);

        var snapshot = new Snapshot(context, state, payload);
        stack.Push(snapshot);

        return Task.FromResult(snapshot);
    }

    public async Task SetResult(Guid id, object result)
    {
        GuardAgainstCallBeforeInitialization();
        
        var currentSnapshot = await Get(id);
        var newSnapshot = currentSnapshot with { Result = result };

        var stack = GetStack(id);
        if (!stack.TryPop(out _))
        {
            throw new Exception("Absolute panic! There is a concurrency exception in the snapshot stack!");
        }
        stack.Push(newSnapshot);
    }

    public async Task Remove(Guid id, string state)
    {
        if ((await Get(id)).State == state)
        {
            var stack = GetStack(id);
            if (!stack.TryPop(out _))
            {
                throw new Exception("Absolute panic! There is a concurrency exception in the snapshot stack!");
            }
        }
    }

    private void GuardAgainstCallBeforeInitialization()
    {
        if (!_isInitialized) 
        {
            throw new InvalidOperationException("The snapshot store has not been initialized yet.");
        }
    }

    private ConcurrentStack<Snapshot> GetOrCreateStack(Guid id)
    {
        if (StackExists(id))
        {
            return GetStack(id);
        }

        var stack = new ConcurrentStack<Snapshot>();
        _snapshotStacks.Add(id, stack);

        return stack;
    }

    private bool StackExists(Guid id)
    {
        return _snapshotStacks.ContainsKey(id);
    }

    private bool SnapshotExists(Guid id, string state)
    {
        return StackExists(id) &&
            _snapshotStacks[id].TryPeek(out var snapshot) &&
            snapshot.State == state;
    }

    private ConcurrentStack<Snapshot> GetStack(Guid id)
    {
        return _snapshotStacks[id];
    }
}
