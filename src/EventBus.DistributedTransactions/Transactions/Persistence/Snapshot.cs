namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence;

internal readonly record struct Snapshot
{
    public Guid Id => Context.InstanceId;
    public Context Context { get; init; }
    public string State { get; init; }
    public object Payload { get; init; }
    public object? Result { get; init; }

    public Snapshot(
        Context context,
        string state,
        object payload,
        object? result = null
    )
    {
        Context = context;
        State = state;
        Payload = payload;
        Result = result;
    }

    public override string? ToString()
    {
        return $"{Context.Key}.{State} {Id.ToString("N")[^5..]}";
    }

    public static explicit operator EventMessage(Snapshot snapshot)
    {
        return new(
            instanceId: snapshot.Context.InstanceId,
            transactionKey: snapshot.Context.Key,
            state: snapshot.State,
            payload: snapshot.Payload
        );
    }
}
