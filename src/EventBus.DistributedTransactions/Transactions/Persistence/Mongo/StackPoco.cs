using MongoDB.Bson.Serialization.Attributes;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence.Mongo;

internal class StackPoco
{
    [BsonId]
    public Guid InstanceId { get; set; }

    [BsonElement("snapshots")]
    public List<SnapshotPoco> Snapshots { get; set; } = new List<SnapshotPoco>();

    public StackPoco(Guid instanceId, List<SnapshotPoco> snapshots)
    {
        InstanceId = instanceId;
        Snapshots = snapshots;
    }

    public static StackPoco Create(Guid instanceId, SnapshotPoco snapshot)
    {
        return new(instanceId, new List<SnapshotPoco>() { snapshot });
    }
}
