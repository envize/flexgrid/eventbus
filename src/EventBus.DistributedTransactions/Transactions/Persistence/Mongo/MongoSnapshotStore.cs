using Envize.FlexGrid.EventBus.DistributedTransactions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence.Mongo;

internal class MongoSnapshotStore : ISnapshotStore
{
    private string _transactionKey = default!;
    private readonly MongoOptions _options;
    private readonly IMongoClient _client;
    private bool _isInitialized = false;

    public MongoSnapshotStore(IMongoClient client, IOptions<MongoOptions> options)
    {
        _client = client;
        _options = options.Value;
    }

    public Task Init(string transactionKey)
    {
        _transactionKey = transactionKey;
        _isInitialized = true;

        return Task.CompletedTask;
    }

    public async Task<bool> Exists(Guid id)
    {
        GuardAgainstCallBeforeInitialization();

        var stack = await GetStack(Filter.ById(id), Project.OnlyId);
        return stack is not null;
    }

    public async Task<Snapshot> Get(Guid id)
    {
        GuardAgainstCallBeforeInitialization();

        var stack = await GetStack(Filter.ById(id) & Filter.HasSnapshot(), Project.Everything);
        var snapshot = stack!.Snapshots.Last(x => !x.IsRolledBack);

        return new(
            context: new Context(_transactionKey, stack.InstanceId),
            state: snapshot.State,
            payload: snapshot.Payload,
            result: snapshot.Result
        );
    }

    public async Task<Snapshot> Create(Context context, string state, object payload)
    {
        GuardAgainstCallBeforeInitialization();

        var newSnapshot = SnapshotPoco.Create(state, payload);

        if (await Exists(context.InstanceId))
        {
            await UpdateStack(
                Filter.ById(context.InstanceId) & Filter.HasSnapshot(),
                Update.PushSnapshot(newSnapshot));
        }
        else
        {
            var stack = StackPoco.Create(context.InstanceId, newSnapshot);
            await CreateStack(stack);
        }

        return new(
            context: context,
            state: state,
            payload: payload
        );
    }

    // todo: THIS CALL IS SLOW, usually 6ms
    public async Task SetResult(Guid id, object result)
    {
        GuardAgainstCallBeforeInitialization();

        var stack = await GetStack(Filter.ById(id), Project.OnlySnapshots);
        var snapshots = stack!.Snapshots;
        var snapshot = snapshots.Last();

        snapshot.IsCompleted = true;
        snapshot.Result = result;

        await UpdateStack(
            Filter.ById(id) & Filter.HasSnapshot(),
            Update.Snapshots(snapshots));
    }

    public async Task Remove(Guid id, string state)
    {
        GuardAgainstCallBeforeInitialization();

        var stack = await GetStack(Filter.ById(id), Project.OnlySnapshots);
        var snapshots = stack!.Snapshots;

        snapshots
            .Single(x => x.State == state)
            .IsRolledBack = true;

        await UpdateStack(
            Filter.ById(id) & Filter.HasSnapshot(),
            Update.Snapshots(snapshots));
    }

    private IMongoCollection<StackPoco> GetCollection()
    {
        return _client
            .GetDatabase(_options.DatabaseName)
            .GetCollection<StackPoco>(_transactionKey);
    }

    private async Task CreateStack(StackPoco stack)
    {
        await GetCollection()
            .InsertOneAsync(stack);
    }

    private async Task<StackPoco?> GetStack(FilterDefinition<StackPoco> filter, ProjectionDefinition<StackPoco> projection)
    {
        var options = new FindOptions<StackPoco>()
        {
            Limit = 1,
            Projection = projection
        };

        var query = await GetCollection()
            .FindAsync(filter, options);

        return await query.FirstOrDefaultAsync();
    }

    private async Task<UpdateResult> UpdateStack(FilterDefinition<StackPoco> filter, UpdateDefinition<StackPoco> update)
    {
        var options = new UpdateOptions()
        {
            IsUpsert = false
        };

        return await GetCollection()
            .UpdateOneAsync(filter, update, options);
    }

    private void GuardAgainstCallBeforeInitialization()
    {
        if (!_isInitialized)
        {
            throw new InvalidOperationException("The snapshot store has not been initialized yet.");
        }
    }
}
