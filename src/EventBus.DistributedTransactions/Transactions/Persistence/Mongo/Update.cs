using MongoDB.Driver;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence.Mongo;

internal static class Update
{
    public static UpdateDefinition<StackPoco> Snapshots(IEnumerable<SnapshotPoco> snapshots)
    {
        return Builders<StackPoco>.Update
            .Set(x => x.Snapshots, snapshots);
    }

    public static UpdateDefinition<StackPoco> PushSnapshot(SnapshotPoco snapshot)
    {
        return Builders<StackPoco>.Update
            .Push(x => x.Snapshots, snapshot);
    }
}
