using MongoDB.Bson.Serialization.Attributes;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence.Mongo;

internal class SnapshotPoco
{
    [BsonElement("state")]
    public string State { get; set; }

    [BsonElement("payload")]
    public object Payload { get; set; }

    [BsonElement("result")]
    public object? Result { get; set; }

    [BsonElement("isCompleted")]
    public bool IsCompleted { get; set; }

    [BsonElement("isRolledBack")]
    public bool IsRolledBack { get; set; }

    public SnapshotPoco(
        string state, 
        object payload, 
        object? result, 
        bool isCompleted, 
        bool isRolledBack
    )
    {
        State = state;
        Payload = payload;
        Result = result;
        IsCompleted = isCompleted;
        IsRolledBack = isRolledBack;
    }

    public static SnapshotPoco Create(string state, object payload)
    {
        return new(
            state: state,
            payload: payload,
            result: null,
            isCompleted: false,
            isRolledBack: false
        );
    }
}
