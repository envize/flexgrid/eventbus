using MongoDB.Driver;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence.Mongo;

internal static class Filter
{
    public static FilterDefinition<StackPoco> ById(Guid id)
    {
        return Builders<StackPoco>.Filter
            .Eq(stack => stack.InstanceId, id);
    }

    public static FilterDefinition<StackPoco> HasSnapshot()
    {
        return Builders<StackPoco>.Filter
            .Where(stack => stack.Snapshots.Any(x => true));
    }
}