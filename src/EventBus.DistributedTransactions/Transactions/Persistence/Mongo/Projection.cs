using MongoDB.Driver;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence.Mongo;

internal static class Project
{
    public static readonly ProjectionDefinition<StackPoco> OnlyId =
        Builders<StackPoco>.Projection
            .Include(stack => stack.InstanceId);

    public static readonly ProjectionDefinition<StackPoco> OnlySnapshots =
        Builders<StackPoco>.Projection
            .Include(stack => stack.Snapshots);

    public static readonly ProjectionDefinition<StackPoco> Everything =
        Builders<StackPoco>.Projection
            .Include(stack => stack.InstanceId)
            .Include(stack => stack.Snapshots);
}
