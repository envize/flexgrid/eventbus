namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence;

internal interface ISnapshotStore
{
    Task Init(string transactionKey);
    Task<bool> Exists(Guid id);
    Task<Snapshot> Get(Guid id);
    Task<Snapshot> Create(Context context, string state, object payload);
    Task SetResult(Guid id, object result);
    Task Remove(Guid id, string state);
}
