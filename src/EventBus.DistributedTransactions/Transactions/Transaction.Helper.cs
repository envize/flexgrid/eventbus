using Envize.FlexGrid.EventBus.DistributedTransactions.Transactions.Persistence;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions;

internal partial class Transaction
{
    private string GetNextState(string state)
    {
        return _details.Events
            .Single(@event => @event.TriggerState == state)
            .ResultState;
    }

    private string GetPreviousState(string state)
    {
        return _details.Events
            .Single(@event => @event.ResultState == state)
            .TriggerState;
    }

    private bool IsEndState(string state)
    {
        return state == _details.EndState;
    }

    private bool IsStartState(string state)
    {
        return state == _details.Events
            .Single(@event => !_details.Events
                .Any(e => @event.TriggerState == e.ResultState))
            .TriggerState;
    }
}
