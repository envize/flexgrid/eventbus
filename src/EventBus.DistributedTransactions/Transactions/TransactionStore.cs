using Microsoft.Extensions.DependencyInjection;

namespace Envize.FlexGrid.EventBus.DistributedTransactions.Transactions;

internal class TransactionStore : ITransactionStore
{
    private readonly IDictionary<string, ITransaction> _transactions;

    public TransactionStore(IServiceProvider provider)
    {
        _transactions = CreateTransactions(provider);
    }

    private IDictionary<string, ITransaction> CreateTransactions(IServiceProvider provider)
    {
        var result = new Dictionary<string, ITransaction>();

        foreach (var details in GetDetails(provider))
        {
            var transaction = provider.GetRequiredService<ITransaction>();
            transaction.Init(details);

            result.Add(details.Key, transaction);
        }

        return result;
    }

    private IEnumerable<TransactionDetails> GetDetails(IServiceProvider provider)
    {
        return provider
            .GetServices<ITransactionController>()
            .Select(c => c.Define());
    }

    public bool Exists(string key)
    {
        return _transactions.ContainsKey(key);
    }

    public ITransaction Get(string key)
    {
        if (!Exists(key))
        {
            throw new ArgumentException($"No transaction is present in the store with key '{key}'.");
        }

        return _transactions[key];
    }

    public bool TryGet(string key, out ITransaction? transaction)
    {
        if (!Exists(key))
        {
            transaction = null;
            return false;
        }

        transaction = Get(key);
        return true;
    }
}
