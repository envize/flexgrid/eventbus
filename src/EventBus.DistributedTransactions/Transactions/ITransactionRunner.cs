namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public interface ITransactionRunner
{
    Task<object> Run(string transactionKey, object payload);
    Task<TResult> Run<TResult>(string transactionKey, object payload);
    Task FireAndForget(string transactionKey, object payload);
}
