namespace Envize.FlexGrid.EventBus.DistributedTransactions;

internal interface ITransaction
{
    Task<TResult> Run<TResult>(object payload);
    Task FireAndForget(object payload);
    Task OnCompletion(EventCompletionMessage message);
    Task OnRollbackCompletion(RollbackCompletionMessage message);
    Task Init(TransactionDetails details);
    Task OnFailure(EventFailureMessage message);
}
// todo: what about dropped messages?
