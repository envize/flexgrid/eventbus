using Envize.FlexGrid.Monitoring.Tracing;

namespace Envize.FlexGrid.EventBus.DistributedTransactions;

public static class SpanExtensions
{
    public static Span SetTransactionMessage(this Span span, IMessage message)
    {
        const string group = "transaction";
        
        span.SetTag($"{group}.id", message.InstanceId);
        span.SetTag($"{group}.key", message.TransactionKey);
        span.SetTag($"{group}.state", message.State);
        span.SetTag($"{group}.operation", GetOperationName(message));

        return span;
    }

    private static MessageNamingService? _naming;

    private static string GetOperationName(IMessage message)
    {
        _naming ??= new();
        return _naming.GetOperationName(message);
    }
}
