using System;
using System.Collections.Generic;

namespace Envize.Yielder.EventBus.MassTransit.RabbitMQ
{
    internal interface IConsumerIntializer
    {
        void SetHandlers(IEnumerable<Type> handlerTypes);
    }
}