using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Envize.Yielder.EventBus.MassTransit.RabbitMQ
{
    public static class RegistrationExtensions
    {
        public static IServiceCollection AddMassTransitRabbitMQEventBus(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            var appConfiguration = provider.GetService<IConfiguration>();

            var rabbitConfiguration = new RabbitConfiguration();
            appConfiguration.Bind(nameof(RabbitConfiguration), rabbitConfiguration);

            return AddMassTransitRabbitMQEventBus(services, rabbitConfiguration);
        }

        public static IServiceCollection AddMassTransitRabbitMQEventBus(this IServiceCollection services, RabbitConfiguration configuration)
        {
            services
                .AddSingleton<RabbitConfiguration>(provider => configuration)
                .AddSingleton<EventBus>()
                .AddSingleton<IEventBus, EventBus>(x => x.GetRequiredService<EventBus>())
                .AddSingleton<IEventBusController, EventBus>(x => x.GetRequiredService<EventBus>())
                .AddSingleton<SubscriptionStore>()
                .AddHostedService<EventBusService>();

            services.RegisterEventHandlers();

            return services;
        }
    }
}