using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.ConsumeConfigurators;

namespace Envize.Yielder.EventBus.MassTransit.RabbitMQ
{
    public class EventBus : IEventBusController
    {
        private readonly RabbitConfiguration _configuration;
        private readonly SubscriptionStore _subscriptions;
        private readonly IServiceProvider _provider;

        private IBusControl? _busController;
        private IBus? _bus;

        public EventBus(
            RabbitConfiguration configuration,
            SubscriptionStore subscriptions,
            IServiceProvider provider)
        {
            _configuration = configuration;
            _subscriptions = subscriptions;
            _provider = provider;
        }

        public async Task Publish<TEvent>(TEvent @event) where TEvent : IntegrationEvent, new()
        {
            await _bus.Publish<TEvent>(@event);
        }

        public Task RegisterHandler<TEvent, THandler>()
            where TEvent : IntegrationEvent, new()
            where THandler : IIntegrationEventHandler<TEvent>
        {
            var eventname = new TEvent().Name;
            _subscriptions.AddSubscription(eventname, typeof(TEvent), typeof(THandler));

            return Task.FromResult(0);
        }

        public async Task Start()
        {
            _busController = Bus.Factory.CreateUsingRabbitMq(configuration =>
            {
                configuration.Host(
                    _configuration.Hostname, 
                    _configuration.VirtualHost, 
                    rabbitConfiguration =>
                {
                    rabbitConfiguration.Username(_configuration.Username);
                    rabbitConfiguration.Password(_configuration.Password);
                });

                var subscriptions = _subscriptions.GetSubscriptions();

                foreach (var entry in subscriptions)
                {
                    var eventname = entry.Key;
                    configuration.ReceiveEndpoint(eventname, rabbitConfiguration =>
                    {
                        var subscription = entry.Value.First();
                        var eventType = subscription.EventType;
                        var handlerTypes = entry.Value.Select(s => s.HandlerType);
                        var consumerType = typeof(ConsumerProxy<>).MakeGenericType(eventType);

                        rabbitConfiguration.Consumer(consumerType, type => {
                            var consumer = (IConsumerIntializer) Activator.CreateInstance(type, _provider);
                            consumer.SetHandlers(handlerTypes);
                            return consumer;
                        });
                    });
                }
            });
            
            var handle = await _busController.StartAsync();
            var ready = await handle.Ready;
            _bus = ready.Bus;
        }

        public async Task Stop()
        {
            if (_busController != null)
            {
                await _busController.StopAsync();
                _busController = null;
            }
        }

        public async ValueTask DisposeAsync()
        {
            await Stop();
        }
    }
}