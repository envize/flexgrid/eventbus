using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;

namespace Envize.Yielder.EventBus.MassTransit.RabbitMQ
{
    internal class ConsumerProxy<TEvent> : IConsumerIntializer, IConsumer<TEvent> where TEvent : class
    {
        public readonly IServiceProvider _provider;
        private IEnumerable<Type> _handlerTypes = Array.Empty<Type>();

        private IEnumerable<object>? _handlers;
        private IEnumerable<object> Handlers 
        {
            get
            {
                _handlers ??= _handlerTypes
                    .Select(type => _provider.GetService(type));
                return _handlers;
            }
        }

        public ConsumerProxy(IServiceProvider provider)
        {
            _provider = provider;
        }

        public async Task Consume(ConsumeContext<TEvent> context)
        {
            foreach (var handler in Handlers)
            {
                var handleMethod = handler.GetType().GetMethod("Handle");
                var task = (Task) handleMethod.Invoke(handler, new[] { context.Message });
                await task.ConfigureAwait(false);
            }
        }

        public void SetHandlers(IEnumerable<Type> handlerTypes)
        {
            _handlerTypes = handlerTypes;
        }
    }
}