namespace Envize.Yielder.EventBus.MassTransit.RabbitMQ
{
    public class RabbitConfiguration
    {
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? Hostname { get; set; }
        public string VirtualHost { get; set; } = "/";
    }
}